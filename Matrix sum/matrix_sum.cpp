#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>

using namespace std;

class Matrix {
private:
	vector<vector<int>> _p_matrix;
	unsigned int rows;
	unsigned int columns;
public:
	Matrix(){}
	Matrix(int num_rows, int num_columns) {
		if(num_rows < 0 || num_columns < 0) {
			throw std::out_of_range("Ошибочное задание диапазона");
		}
		else {
			// _p_matrix.reserve(num_columns);
			for (int i = 0; i < num_columns; i++) {
				// _p_matrix[i].reserve(num_rows);
			}
		}
	}

	size_t Capacity() {
		size_t t = sizeof(vector<int>)/*+sizeof(_p_matrix.capacity())*/;
		for (int i = 0; i < columns; ++i) {
			t += sizeof(vector<int>)+(sizeof(int)* sizeof(_p_matrix[i].capacity()));
		}
		return t;
		
	}

	void Reset(int num_rows, int num_columns) {
		if(num_rows < 0 || num_columns < 0) {
			throw std::out_of_range("Ошибочное задание диапазона");
		}
		else {
			rows = num_rows;
			columns = num_columns;
			cout << this->Capacity() << endl;
			_p_matrix.resize(num_columns);
			_p_matrix.emplace_back(vector<int> {0});
			_p_matrix.shrink_to_fit();
			cout << this->Capacity() << endl;
			for (int i = 0; i < num_columns; ++i) {
				_p_matrix[i].resize(num_columns);//emplace_back(0);
			}
			_p_matrix.shrink_to_fit();

			cout << this->Capacity() << endl;
		}
	}

	void print_matrix () const {
		for(auto &row: _p_matrix) {
			for (auto &cell: row) {
				cout << cell << " ";
			}
			cout << endl;
		}
	}

	int At (const int i, const int j)	const {
		if(i < 0 || j < 0 || i > _p_matrix.size() || j > _p_matrix[i].size()) {
			throw std::out_of_range("Ошибочное задание диапазона");
		}
		auto n = (_p_matrix.at(i)).at(j);
		return _p_matrix.at(i).at(j);
	}

	// vector<vector<int>>& At (const int i, const int j)	{
	// 	if(i < 0 || j < 0 || i > _p_matrix.size() || j > _p_matrix[i].size()) {
	// 		__throw_out_of_range;
	// 	}
	// 	auto n = (_p_matrix.at(i)).at(j);
	// 	return &(_p_matrix.at(i)).at(j);
	// }
};

// Реализуйте здесь
// * класс Matrix
// * оператор ввода для класса Matrix из потока istream
// * оператор вывода класса Matrix в поток ostream
// * оператор проверки на равенство двух объектов класса Matrix
// * оператор сложения двух объектов класса Matrix

int main() {
  Matrix one(-1,1);
  one.Reset(2,2);
  one.print_matrix();
  Matrix two;
	std::cout << "hello world"<<std::endl;
//   cin >> one >> two;
//   cout << one + two << endl;
  return 0;
}
